import java.util.Scanner;

public class updatedShop2{
	
	// UPDATED/BETTER VERSION OF MY CODE WITH INPUTS FROM USER WITH THE CONSTRUCTER

public static void main(String[] args){
		
		//Creating an input for the user
		Scanner reader = new Scanner(System.in);
		
		//Creating an array of 4 objects/movies
		updatedMovie2[] myMovieList = new updatedMovie2[4];
		
		//Populating the array with a for loop
		for (int i = 0; i < myMovieList.length; i++){
			
			//Asking the user to input the title of the movie
			System.out.println("Enter the title of the movie "+ (i+1) + " :");
			String title = reader.next();
			
			//Asking the user to input the release date of the movie
			System.out.println("Enter the release date of the movie "+ (i+1) + " :");
			int releaseDate = reader.nextInt();
			
			//Asking the user to input the director of the movie
			System.out.println("Enter the director of the movie "+ (i+1) + " :");
			String director = reader.next();
			
			//Passing the values entered by the user to the constructer to create the objects in the array
			myMovieList[i] = new updatedMovie2(title, releaseDate, director);
			
			//Making space between the arguments by adding a clear line
			System.out.println();
		}
		
		//Making space between the arguments by adding a clear line
			System.out.println();
		
		//Displaying the old title of the 4th/last movie-BEFORE
		 System.out.println("The old title of the last movie: "+ myMovieList[3].getTitle());
		 
		 //Displaying the old release date of the 4th/last movie-BEFORE
		System.out.println("The old release date of the last movie: "+ myMovieList[3].getReleaseDate());
		
		//Displaying the old director of the 4th/last movie-BEFORE
		System.out.println("The old director of the last movie: "+ myMovieList[3].getDirector());
		
		//Making space between the arguments by adding a clear line
			System.out.println();
		
		
		/*
		Asking the user to input new values of the 3 fields for the last movie object
		Calling the setter methods for the 3 fields
		*/
		System.out.println("Enter a new title for the last movie: ");
		myMovieList[3].setTitle(reader.next());
		System.out.println("Enter a new release date for the last movie: ");
		myMovieList[3].setReleaseDate(reader.nextInt());
		System.out.println("Enter a new director for the last movie: ");
		myMovieList[3].setDirector(reader.next());
		 
		 //Making space between the arguments by adding a clear line
			System.out.println();
		 
		 //Displaying the new title of the 4th/last movie
		 System.out.println("The new title of the last movie: "+ myMovieList[3].getTitle());
		 
		 //Displaying the new release date of the 4th/last movie
		System.out.println("The new release date of the last movie: "+ myMovieList[3].getReleaseDate());
		
		//Displaying the new director of the 4th/last movie
		System.out.println("The new director of the last movie: "+ myMovieList[3].getDirector());
		
		//Calling the printTitle instance method from the Movie class to display the title of the 4th/last movie 
		myMovieList[3].printTitle();
	}
}