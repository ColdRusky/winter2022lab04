public class updatedMovie2{

//Creating 3 private fields
	private String title;
	private int releaseDate;
	private String director;
	
	//Creating getter methods for the 3 fields
	public String getTitle(){
		  return this.title;
		  }
	public int getReleaseDate(){
		  return this.releaseDate;
		  }
	public String getDirector(){
		  return this.director;
		  }
	
	//Creating setter methods for the 3 fields
	public void setTitle(String newTitle){
		 this.title = newTitle;
		  }
		  
	public void setReleaseDate(int newReleaseDate){
		 this.releaseDate = newReleaseDate;
		  }
		  
	public void setDirector(String newDirector){
		 this.director = newDirector;
		  }
		  
		  //An instance method that uses 1 of the fields and returns void             
	public void printTitle(){
		
		//Displaying the title field of the 4th/last movie to the user
		System.out.println("This is the title of the last movie: "+ this.title);
	}
	
	//Creating a constructor with 3 inputs for the 3 fields
	 
	public updatedMovie2(String title, int releaseDate, String director) {
		this.title = title;
		this.releaseDate = releaseDate;
		this.director = director;
		}
	
}